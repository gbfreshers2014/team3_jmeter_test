var isAssertionFailed = false;
var failureMessage = "";

var responseCode = prev.getResponseCode();

if(!responseCode.equals("200"))
{
	failureMessage = "Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200" ;
	isAssertionFailed = true;
}
else
{
	OUT.println("[JMETER_INFO] [" + sampler.getName() + "] [" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");

	try
	{
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		failureMessage = "Response is not a valid JSON Object" ;
		isAssertionFailed = true;
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && (apiResponse instanceof Array))
	{

                var items = apiResponse.length
		OUT.println("[JMETER_INFO] [" + sampler.getName() + "] [" + sampler.getUrl() + "] ====> [" + "Found " + items + " Inventories ]");
		if(items <= 0) {
			failureMessage = "No Items Returned. Items: " + items + ", Expecting: >0" ;
			isAssertionFailed = true;
		}

if(isAssertionFailed)
{
	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
}
