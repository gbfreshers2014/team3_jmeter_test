var myAssertion = {

  assertError:function(failureMessage)
  {
  	OUT.println("[JMETER_ERROR] [" + sampler.getName() + "] [" + sampler.getUrl() + "], AssertionFailureMessage [" + failureMessage + "]");
  	AssertionResult.setFailureMessage(failureMessage) ;
	AssertionResult.setFailure(true);
  },
  
  assertPass:function(logMessage)
  {
	// OUT.println("[JMETER_PASSED] [" + sampler.getName() + "] " + logMessage);
  },
  
  logDebug:function(logMessage)
  {
	OUT.println("[JMETER_DEBUG] [" + sampler.getName() + "] " + logMessage);
  },
	validateInventory:function(param_inventory, response_inventory)
  {
		//Verifying inventory object now
		for (var key in param_inventory) 
		{
  			if (param_inventory.hasOwnProperty(key) && key != 'issuedTo' && typeof param_inventory[key] !== 'object')
  			{
  			    var responseValue = response_inventory[key];
				var paramExpectedValue = param_inventory[key];
				
				if(typeof responseValue === 'undefined' || responseValue == null || String(responseValue) !== String(paramExpectedValue))
				{
					myAssertion.assertError("response_inventory does not have valid " + key + ". Received: " + responseValue + ", Expecting: " + paramExpectedValue);
				}
				else
				{
					myAssertion.assertPass("Verifying inventory Key: " + key + ", Expecting: " + paramExpectedValue+", Received: " + responseValue);
				}
  			}
		}
			
  },
};

var paramsLength = Parameters.length;
log.info("**********" + Parameters)
var responseCode = prev.getResponseCode();

if(!responseCode.equals("200"))
{
	myAssertion.assertError("Invalid ResponseCode received. ResponseCode: " + responseCode + ", Expecting: 200");
}
else
{
	myAssertion.logDebug("[" + sampler.getUrl() + "] ====> [" + prev.getResponseDataAsString() + "]");
	
	try
	{
		eval('var parametersJSON = ' + Parameters);
		eval('var apiResponse = ' + prev.getResponseDataAsString());	
	}	 
	catch (e)
	{
		myAssertion.assertError("Response is not a valid JSON Object");
	}
	
	if(typeof apiResponse !== 'undefined' && apiResponse && !AssertionResult.isFailure())
	{		
		var statusJson = apiResponse['status'];
		var code = statusJson['code'];
		var message = statusJson['message'];
		
		if(typeof code === 'undefined' || code == null || code !== 0)
		{
			myAssertion.assertError("Code in JSON response is invalid. Code: " + code + ", Expecting: 0");
		}
		else
		{
			myAssertion.assertPass("Verifying status Code: " + code + ", Expecting: 0");
		}
		
		if(typeof message === 'undefined' || message == null || message.length <= 0 || message != 'success in getting the details from Transection and invetory tables')
		{
			myAssertion.assertError("Error message in JSON response is not valid. Message: " + message + ", Expecting: success in getting the details from Transection and invetory tables");
		} 
		else
		{
			myAssertion.assertPass("Verifying Message: " + message + ", Expecting: success");
		}
		//Verify inventory
		var param_inventorys = parametersJSON['inventorys'];
		var response_inventorys = apiResponse['inventorys'];

		var response_inventory = response_inventorys[0];
		var param_inventory = param_inventorys[0];
		if(typeof param_inventory !=='undefined' && param_inventory && typeof response_inventory !=='undefined' && response_inventory)
		{
			myAssertion.validateInventory(param_inventory, response_inventory);
		}
		else
		{
			myAssertion.assertError("One of param or response inventory JSON is not valid. param_inventory: " + param_inventory + ", response_inventory: " + response_inventory);
		}
		
	}
	else
	{
		failureMessage = "apiResponse is not a valid object. apiResponse: " + apiResponse;
		isAssertionFailed = true;
	}
}
